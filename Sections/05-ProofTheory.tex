\section{Proof Theory}

The sequent calculus LJ is used to carry out derivations in intuitionistic logic.

\begin{definition}[Sequent Calculus LJ]
    \it{\tbd}.
\end{definition}

The sequent calculus LK is used to derive proofs in classical logic.

\begin{definition}[Sequent Calculus LK]
    \it{\tbd}.
\end{definition}

The reason that the set of formulae $\Delta$ is allowed to be carried over in the right-hand side of sequents is that alternative modifications to LJ lead to loss of properties in the sequent calculus. Specifically

\begin{itemize}
    \item The introduction of the RAA rule

	\begin{prooftree}
	    \AxiomC{$\Gamma, \lnot A \vdash$}
	    \UnaryInfC{$\Gamma \vdash A$}
	\end{prooftree}

	leads to the loss of the subformula property
    \item The introduction of the $\vdash A \lor \lnot A$ axiom necessitates the presence of the cut rule
\end{itemize}

The Sequent Calculus LK' permits to derive proofs in an automated way

\begin{definition}[Sequent Calculus LK']
    \it{\tbd}.
\end{definition}

\begin{proposition}
    Everything that appears in the premises of a sequent calculus rule also appears in the conclusion.\footnote{This doesn't seem to be strictly sure. Ask someone or annotate this in the other document.}
\end{proposition}

\begin{proof}
    \it{Prove only if required at the exam.}
\end{proof}

\begin{theorem}[Soundness of LK']
    If $\Gamma \vdash_{LK'} \Delta$ is derivable then $\Gamma \vDash \Delta$.
\end{theorem}

\begin{proof}
    By induction on the depth $n$ of the derivation tree of $\Gamma \vdash \Delta$.

    If $n = 0$, then $\exists A \in \Delta ~|~ A \in \Gamma$, which trivially means $\Gamma \vDash \Delta$.

    If, on the other hand, $n > 0$, we can proceed case-by-case on the last rule applied. For example, if we consider

    \begin{prooftree}
	\AxiomC{\vdots}
	\UnaryInfC{$\Gamma, A \vdash \Delta$}
	\AxiomC{\vdots}
	\UnaryInfC{$\Gamma, B \vdash \Delta$}
	\RightLabel{$(\lor l)$}
	\BinaryInfC{$\Gamma, A \lor B \vdash \Delta$}
    \end{prooftree}

    then we have that, since the proof trees of $\Gamma, A \vdash \Delta$ and $\Gamma, B \vdash \Delta$ have depth $< n$, by the IH

    \begin{align}
	\label{eq.ih.1}
	\Gamma, A & \vDash \Delta \\
	\label{eq.ih.2}
	\Gamma, B & \vDash \Delta
    \end{align}

    To prove $\Gamma, A \lor B \vDash \Delta$\footnotemark, suppose to have an interpretation $v$ that satisfies $\Gamma, A \lor B$. Then

    \begin{itemize}
	\item If $v(A) = 1$ but $v(B) = 0$, by \eqref{eq.ih.1}, $\Gamma, A \lor B \vDash \Delta$
	\item If $v(B) = 1$ but $v(A) = 0$, by \eqref{eq.ih.2}, $\Gamma, A \lor B \vDash \Delta$
    \end{itemize}

    \footnotetext{This is an alternative proof as the one presented in the course slides.}

    \textbf{Consider adding more examples.}
\end{proof}

\begin{lemma}
    For each rule of the SC LK', given a derivation

    \begin{prooftree}
	\AxiomC{$\Gamma_1 \vdash \Delta_1$}
	\AxiomC{$\cdots$}
	\AxiomC{$\Gamma_n \vdash \Delta_n$}
	\TrinaryInfC{$\Gamma \vdash \Delta$}
    \end{prooftree}

    if an interpretation $v$ validates the conclusion $\Gamma \vdash \Delta$, that is $\Gamma \vDash \Delta$, then $v$ also validates the premises of the inference rule, namely $\forall i$

    \begin{equation}
        \Gamma_i \vDash \Delta_i
    \end{equation}
\end{lemma}

\begin{proof}
    The complete proof of the statement can be carried out going through each of the rules of the LK' system. For the sake of brevity, only a handful of these are considered here.

    Consider the rule

    \begin{prooftree}
	\AxiomC{$\Gamma, A \vdash \Delta$}
	\AxiomC{$\Gamma, B \vdash \Delta$}
	\RightLabel{$\lor l$}
	\BinaryInfC{$\Gamma, A \lor B \vdash \Delta$}
    \end{prooftree}

    and suppose that $\Gamma, A \lor B \vDash \Delta$. This means that either $v(\Gamma, A \lor B) = 0$ or $v(\Delta) = 1$

    \begin{itemize}
        \item If $v(\Gamma) = 0$, then $v(\Gamma, A) = v(\Gamma, B) = 0$
	\item If $v(A \lor B) = 0$, then $v(\Gamma, A) = v(\Gamma, B) = 0$
        \item If $v(\Delta) = 1$, then (with a slight abuse of notation) $v(\Gamma, A \implies \Delta) = 1$ and $v(\Gamma, B \implies \Delta) = 1$
    \end{itemize}

    As a further example, consider the rule

    \begin{prooftree}
	\AxiomC{$\Gamma, A \vdash B, \Delta$}
	\RightLabel{$(\implies r)$}
	\UnaryInfC{$\Gamma \vdash A \implies B, \Delta$}
    \end{prooftree}

    Suppose again that $\Gamma \vdash A \implies B, \Delta$, then given an interpretation $v$, either $v(\Gamma) = 0$ or $v(A \implies B, \Delta) = 1$. When $v(\Gamma) = 0$ or $v(\Delta) = 1$, it is easy to argue that $v(\Gamma, A \vDash B, \Delta) = 1$.

    The more interesting case is when $v(A \implies B) = 1$, this means that either $v(A) = 0$ or $v(B) = 1$. By going through these two cases and remembering that $B, \Delta$ is semantically equivalent to $B \lor \Delta$, one gets the desired result.
\end{proof}

\begin{observation}
    The previous result is valid because of the properties of LK', and does not hold, for instance, in LK. To see this, consider the rule

    \begin{prooftree}
	\AxiomC{$\Gamma \vdash A_i, \Delta$}
	\RightLabel{$(\lor r)_i$}
	\UnaryInfC{$\Gamma \vdash A_1 \lor A_2, \Delta$}
    \end{prooftree}

    Suppose then that $\Gamma \vDash A_1 \lor A_2, \Delta$, and in particular that, for a given interpretation $v$, $v(A_1 \lor A_2, \Delta) = 1$. All is fine if $v(\Gamma) = 0$, but if $v(\Gamma) = 1$, then there are no means to prove that $v(A_1) = 1$ or $v(A_2) = 1$ specifically.
\end{observation}

\begin{lemma}
    For any rule $r$ of the SC LK', if an interpretation $v$ validates the conclusion $\Gamma \vdash \Delta$ of $r$, then $v$ also validates the premises $\Gamma_1 \vdash \Delta_1, \ldots, \Gamma_n \vdash \Delta_n$ of $r$.
\end{lemma}

\begin{theorem}[Completeness of LK']
    If $\Gamma \vDash \Delta$ then $\Gamma \vdash_{\mathrm{LK'}} \Delta$ is derivable.
\end{theorem}

\begin{proof}
    \it{\tbd}.
\end{proof}

\begin{corollary}
    For the sequent calculus (LK'?)

    \begin{itemize}
        \item The contraction rules are redundant
	\item If the axioms are sequents $\Gamma \vdash \Delta$ such that $\Gamma \cap \Delta \neq \emptyset$, then the weakening rules are redundant
	\item The cut rule is redundant
    \end{itemize}
\end{corollary}

\begin{theorem}
    $\Gamma \vdash_{\mathrm{SC}} \Delta \iff \Gamma \vdash_{\mathrm{HS}} P$.
\end{theorem}

\begin{proof}
    For the $\impliedby$ part, it suffices to show that every axiom in the Hilbert system can be expressed in a sequent calculus proof tree. In particular, the MP rule can be expressed as

    \begin{prooftree}
		\AxiomC{$\vdash A$}
		    \AxiomC{$\vdash A \implies B$}

			\AxiomC{$A \vdash A$}
		    \UnaryInfC{$A \vdash A, B$}

			\AxiomC{$B \vdash B$}
		    \UnaryInfC{$B, A \vdash B$}
		\BinaryInfC{$A \implies B, A \vdash B$}
	    \BinaryInfC{$A \vdash B$}
	\BinaryInfC{$\vdash B$}
    \end{prooftree}

    For the $\implies$, we proceed by induction on the depth $n$ of the derivation tree for $\Gamma \vdash_{\mathrm{SC}} \Delta$, as we already did in the proof of equivalence between natural deduction and Hilbert systems.

    If $n = 1$, then $\exists P \in \Delta ~|~ P \in \Gamma$, meaning that $\Gamma \vdash_{\mathrm{HS}} P$ because of the $\vdash A \implies A$ axiom.

    If, on the other hand, $n > 1$, then we consider the last rule in the derivation of $\Gamma \vdash \Delta$, exhausting all possibilities by means of a case-by-case analysis. For example, in the case of the tree

    \begin{prooftree}
	\AxiomC{\vdots}
	\UnaryInfC{$\Gamma, A \vdash \Delta$}

	\AxiomC{\vdots}
	\UnaryInfC{$\Gamma, B \vdash \Delta$}

	\RightLabel{$(\lor l)$}
	\BinaryInfC{$\Gamma, A \lor B \vdash \Delta$}
    \end{prooftree}

    we have that the subtrees rooted in $\Gamma, A \vdash \Delta$ and $\Gamma, B \vdash \Delta$ have depth $< n$, so by the IH $\Gamma, A \vdash_{\mathrm{HS}} \Delta$ and $\Gamma, B \vdash_{\mathrm{HS}} \Delta$. By the deduction theorem, this further means that

    \begin{align}
	\Gamma & \vdash A \implies \Delta \\
	\Gamma & \vdash B \implies \Delta
    \end{align}

    so that we can construct the following HS derivation for $\Gamma, A \lor B \vdash \Delta$

    {
	\footnotesize
	\begin{prooftree}
	    \AxiomC{$B \implies \Delta$}

	    \AxiomC{$A \implies \Delta$}
	    \AxiomC{$(A \implies C) \implies ((B \implies C) \implies ((A \lor B) \implies C))$}
	    \UnaryInfC{$(A \implies \Delta) \implies ((B \implies \Delta) \implies ((A \lor B) \implies \Delta))$}
	    \BinaryInfC{$(B \implies \Delta) \implies ((A \lor B) \implies \Delta)$}

	    \BinaryInfC{$(A \lor B) \implies \Delta$}
	\end{prooftree}

	\qedhere
    }
\end{proof}
