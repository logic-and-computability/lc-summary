\section{Proof Theory}

\begin{proposition}
    \label{prop.impl}
    In an Hilbert System equipped with the axioms

    \begin{align}
	A & \implies A \tag{I} \\
	B & \implies (A \implies B) \tag{K} \\
	(A \implies (C \implies B)) & \implies ((A \implies C) \implies (A \implies B)) \tag{S}
    \end{align}

    if $A \vdash_{\mathrm{HS}} B$ then

    \begin{equation}
	\vdash_{\mathrm{HS}} A \implies B
    \end{equation}
\end{proposition}

\begin{proof}
    By induction on the number $n$ of applications of the MP rule.

    If $n = 0$, then $A \vdash B$ is obtained with no applications of MP. In this scenario, two cases are possible

    \begin{itemize}
        \item Either $A = B$
	\item Or $A \neq B$, with $B$ being an axiom
    \end{itemize}

    In the first case, there is nothing to prove, as $\vdash A \implies A$ is already a theorem. In the second, we can provide the following proof:\footnotemark

    \begin{prooftree}
	\AxiomC{B}
	\AxiomC{$B \implies (A \implies B)$}
	\RightLabel{MP}
	\BinaryInfC{$A \implies B$}
    \end{prooftree}

    Note that we exploited the fact that $B$ is an axiom which we can use.

    \footnotetext{Do not be misled by the fact that "there are $0$ applications of MP". MP is applied $0$ times in the proof of $A \vdash B$, but there are no restrictions on the number of MP applications for the proof of $\vdash A \implies B$.}

    In the inductive step, $A \vdash B$ is obtained with $n > 0$ applications of MP. Since HS derivations are carried out by the sole application of MP, the last portion of the derivation tree has to look like:

    \begin{prooftree}
	\AxiomC{A}
	\noLine
	\UnaryInfC{$\vdots$}
	\noLine
	\UnaryInfC{$C \implies B$}

	\AxiomC{A}
	\noLine
	\UnaryInfC{$\vdots$}
	\noLine
	\UnaryInfC{C}
	\RightLabel{MP}
	\BinaryInfC{B}
    \end{prooftree}

    By the inductive hypothesis, we then have that $\vdash A \implies (C \implies B)$ and $\vdash A \implies C$ are derivable, as they can be obtained with less than $n$ applications of MP. This allows us to express the following proof for $\vdash A \implies B$:

	\begin{prooftree}
	    \AxiomC{A}
	    \noLine
	    \UnaryInfC{$\vdots$}
	    \noLine
	    \UnaryInfC{$A \implies (C \implies B)$}

	    \AxiomC{$\overbrace{(A \implies (C \implies B)) \implies \ldots}^{S}$}

	    \BinaryInfC{$(A \implies C) \implies (A \implies B)$}

	    \AxiomC{A}
	    \noLine
	    \UnaryInfC{$\vdots$}
	    \noLine
	    \UnaryInfC{$A \implies C$}

	    \RightLabel{MP}
	    \BinaryInfC{$A \implies B$}
	\end{prooftree}
\end{proof}

\begin{theorem}
    If $A_1, \ldots, A_{n - 1}, A_n \vdash_{\mathrm{HS}}$ then $A_1, \ldots, A_{n - 1} \vdash_{\mathrm{HS}} A \implies B$.
\end{theorem}

\begin{proof}
    Similarly to the proof of Prop. \ref{prop.impl}.
\end{proof}

\begin{proposition}
    $K, S \vdash_{HS} I$.
\end{proposition}

\begin{proof}
    We have

    {\tiny
	\begin{prooftree}
	    \AxiomC{
		$
		\begin{aligned}
		& [A \implies (C \implies B)] \implies \\
		& [(A \implies C) \implies (A \implies B)]
		\end{aligned}
		$
	    }
	    \UnaryInfC{
		$[A \implies ( (A \implies A) \implies A)] \implies \ldots$
	    }

	    \AxiomC{$B \implies (A \implies B)$}
	    \UnaryInfC{\ldots}

	    \BinaryInfC{$(A \implies (A \implies A)) \implies (A \implies A)$}

	    \AxiomC{$B \implies (A \implies B)$}
	    \UnaryInfC{\ldots}

	    \BinaryInfC{$A \implies A$}
	\end{prooftree}
    }
\end{proof}

\begin{theorem}
    Given a set of WFFs $\Gamma$

    \begin{equation}
	\Gamma \vdash_{\mathrm{ND}} P \iff \Gamma \vdash_{\mathrm{HS}} P
    \end{equation}
\end{theorem}

\begin{proof}[Proof]
    \halfdone~For the $\impliedby$ direction, given that MP is also a rule of ND, it suffices to show that all the axioms of HS can be proven in ND. With these theorems and the MP rule, one can derive $\Gamma \vdash_{\mathrm{ND}} P$ just as one derived $\Gamma \vdash_{\mathrm{HS}} P$.

    For the $\implies$ we instead proceed by induction on the depth $n$ of the derivation tree of the proof $\Gamma \vdash_{\mathrm{ND}} P$.

    If $n = 1$, then $P \in \Gamma$, meaning that $\Gamma \vdash_{\mathrm{HS}} P$ trivially.

    If $n > 1$, we go by cases on the last rule used to derive $P$. For example, if we use $A_1 \land A_2 \vdash_{\mathrm{ND}} A_k$, we consider the subtrees used to obtain the proofs of $\Gamma \vdash_{\mathrm{ND}} A_1$ and $\Gamma \vdash_{\mathrm{ND}} A_2$; these trees have depth $n - 1$, and by the IH $\Gamma \vdash_{\mathrm{HS}} A_1$ and $\Gamma \vdash_{\mathrm{HS}} A_2$. By using these two derivations and the $A_1 \land A_2 \implies A_k$ axiom, we also get a proof for $\Gamma \vdash_{\mathrm{HS}} P$.
\end{proof}
