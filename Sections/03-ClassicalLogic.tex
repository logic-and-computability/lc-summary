\section{Classical Logic}

\begin{definition}[TERM]
    \label{def.term}
    TERM is the smallest set $Y$ such that

    \begin{itemize}
        \item All constants $c \in Y$
        \item All variables $x \in Y$
        \item If $t_1, \ldots, t_n \in Y$ and $f$ is a function symbol of ariety $n$, $f(t_1, \ldots, t_n) \in Y$
    \end{itemize}
\end{definition}

\begin{definition}[WFF]
    WFF is the smallest set $X$ such that

    \begin{itemize}
        \item $\bot \in X$
        \item If $t_1, \ldots, t_n \in \mathrm{TERM}$ and $A$ is a predicate symbol of ariety $n$, then $A(t_1, \ldots, t_n) \in X$
        \item If $P \in X$ then $(\lnot P) \in X$
        \item If $P, Q \in X$ then $P \circ Q \in X$ for $\circ \in \left \{ \land, \lor, \implies \right \}$
        \item If $P \in X$ then $\forall x P, \exists x P \in X$
    \end{itemize}
\end{definition}

\begin{theorem}[Structural induction]
    \label{thm.si.term}
    A property $A(t)$ holds for every term $t \in \mathrm{TERM}$ if

    \begin{enumerate}
        \item $A(x)$ holds for all constants and variables $x$
	\item $\forall t_1, \ldots, t_n \in \mathrm{TERM}$ and all function symbols $f$ of ariety $n$

	    \begin{equation}
		\label{eq.term2}
	        A(t_1), \ldots, A(t_n) \implies A\left ( f(A(t_1), \ldots, A(t_n)) \right )
	    \end{equation}
    \end{enumerate}
\end{theorem}

\begin{proof}
    Consider the set

    \begin{equation}
        Y = \left \{ t \in \mathrm{TERM} ~|~ A(t) \right \}
    \end{equation}

    We want to show that $\mathrm{TERM} \subseteq Y$. To do this, consider the three properties of Definition \ref{def.term}. We have that

    \begin{enumerate}
        \item If $t$ is a constant, then $t \in Y$
        \item If $t$ is a variable, then $t \in Y$
	\item If $t_1, \ldots, t_n \in Y$, then $t_1, \ldots, t_n \in \mathrm{TERM}$ and $A(t_1), \ldots, A(t_n)$. If we take an arbitrary function symbol $f$ and apply \eqref{eq.term2}, we then have

	    \begin{align}
		A & \left ( f(t_1, \ldots, t_n) \right ) \\
		f & (t_1, \ldots, t_n) \in Y
	    \end{align}
    \end{enumerate}

    Therefore $Y$ satisfies all properties of Definition \ref{def.term} and, since $\mathrm{TERM}$ is the smallest set satisfying the same conditions, $\mathrm{TERM} \subseteq Y$. Clearly $Y \subseteq \mathrm{TERM}$, therefore $Y = \mathrm{TERM}$.
\end{proof}

\begin{theorem}
    A property $A(P)$ holds $\forall P \in \mathrm{WFF}$ if

    \begin{itemize}
        \item $\ldots$
    \end{itemize}
\end{theorem}

\begin{proof}
    Similarly to the proof of Theorem \ref{thm.si.term}.
\end{proof}

\begin{definition}[Substitutions]
    Let $s, t \in \mathrm{TERM}$. The substitution of $t$ in place of $x$, denoted $s[t/x]$, is defined as follows:

    \begin{itemize}
        \item If $s$ is a variable

	    \begin{equation}
		s[t/x] =
		\begin{cases}
		    s & \quad \text{if $s \neq x$} \\
		    t & \quad \text{otherwise}
		\end{cases}
	    \end{equation}

	\item If $s$ is a constant

	    \begin{equation}
		c[t/x] = c
	    \end{equation}

	\item For any function symbol $f$

	    \begin{equation}
		f(t_1, \ldots, t_n)[t/x] = f \left ( t_1[t/x], \ldots, t_n[t/x] \right )
	    \end{equation}
    \end{itemize}
\end{definition}

\begin{definition}[Substitutions over WFF]
    Let $P \in \mathrm{WFF}$ and $t \in \mathrm{TERM}$. The substitution of $t$ in place of $x$ inside $P$, denoted $P[t/x]$, is defined as follows:

    \begin{itemize}
        \item $\bot$ \ldots ~ easy
        \item $A(t_1, \ldots, t_n)$ \ldots ~ easy
        \item $\lnot P$ \ldots ~ easy
        \item $(P_1 \circ P_2)$ for $\circ \in \left \{ \land, \lor, \implies \right \}$ \ldots ~ easy
	\item For any $Q \in \left \{ \exists, \forall \right \}$

	    \begin{equation*}
		(Qy P)[t/x] =
		\begin{cases}
		    Qy (P[t/x]) & \quad \text{$x \neq y$ and $y \notin \mathrm{FV}(t)$} \\
		    Qz (P[z/y][t/x]) & \quad \text{$x \neq y$, $y \in \mathrm{FV}(t)$ and $z \notin \mathrm{FV}(P)$} \\
		    Qy P & \quad \text{$x = y$}
		\end{cases}
	    \end{equation*}
    \end{itemize}
\end{definition}

\begin{definition}[Structure]
    A \emph{structure} is a pair $\mathcal{A} = (D, I)$ where $D$ is a non-empty set and $I$ is a function that associates to each symbol a corresponding object. More specifically

    \begin{itemize}
        \item Given any constant symbol $c$, $I(c)$ is an element from $D$
	\item Given any function symbol $f$ of arity $n$, $I(f)$ is a function $D^n \to D$
	\item Given any predicate symbol $A$ of arity $n$, $I(A)$ is a subset of $D^n$ whose elements evaluate to true
    \end{itemize}
\end{definition}

\begin{definition}[Environment]
    Given a structure $\mathcal{A}$, an environment $\xi$ (for $\mathcal{A}$) is a function $\mathrm{VAR} \to D$.
\end{definition}

\begin{definition}[Interpretation]
    Given a first-order language $\mathcal{L}$, an interpretation for $\mathcal{L}$ is a pair $(\mathcal{A}, \xi)$, where $\mathcal{A}$ is a structure and $\xi$ an environment.
\end{definition}

\begin{lemma}
    \label{lemma.eq.term}
    Let $t \in \mathrm{TERM}$ such that $\mathrm{FV} = \left \{ x_1, \ldots, x_n \right \}$ and let $\mathcal{A}$ be a structure. Given two environments $\xi_1$ and $\xi_2$ such that $\forall i, \xi_1(x_i) = \xi_2(x_i)$, one has

    \begin{equation}
	[t]_{\xi_1} = [t]_{\xi_2}
    \end{equation}
\end{lemma}

\begin{lemma}
    Let $P$ be a WFF such that $\mathrm{FV}(P) = \left \{ x_1, \ldots, x_n \right \}$ and let $\mathcal{A} = (D, I)$ be a structure. Given two environments $\xi_1$ and $\xi_2$ such that $\forall i, \xi_1(x_i) = \xi_2(x_i)$, one has

    \begin{equation}
	[P]_{\xi_1} = [P]_{\xi_2}
    \end{equation}
\end{lemma}

\begin{proof}
    By structural induction over the syntax of the set WFF.

    For the base case, one has:

    \begin{itemize}
	\item If $P = \bot$, $[P]_{\xi_1} = [P]_{\xi_2} = 0$ by the definition of interpretation
	\item If $P = A(t_1, \ldots, t_n)$, then

	    \begin{align}
		[A(t_1, \ldots, t_n)]_{\xi_1} &= I(A)(\xi_1(t_1), \ldots, \xi_1(t_n)) \\
					      &= I(A)(\xi_2(t_1), \ldots, \xi_2(t_n)) \tag{Lemma \eqref{lemma.eq.term}} \\
					      &= [A(t_1), \ldots, A(t_n)]_{\xi_2}
	    \end{align}
    \end{itemize}

    For the inductive step, one instead has that:

    \begin{itemize}
	\item If $P = \lnot P'$, then...
	\item If $[P_1]_{\xi_1} = [P_1]_{\xi_2}$ and $[P_2]_{\xi_1} = [P_2]_{\xi_2}$, then

	    \begin{align}
		[P_1 \land P_2]_{\xi_1} &= \min \left \{ [P_1]_{\xi_1}, [P_2]_{\xi_1} \right \} \\
					&= \min \left \{ [P_1]_{\xi_2}, [P_2]_{\xi_2} \right \} \\
					&= [P_1 \land P_2]_{\xi_2}
	    \end{align}
	\item If $P = P_1 \lor P_2$... similarly to $P_1 \land P_2$
	\item If $P = P_1 \implies P_2$... similarly to $P_1 \land P_2$
	\item If $[P]_{\xi_1} = [P]_{\xi_2}$ then

	    \begin{align}
		[\forall x P]_{\xi_1} &= \min \left \{ \left [ P [a/x] \right ]_{\xi_1} ~|~ a \in D \right \} \\
				      &= \min \left \{ \left [ P [a/x] \right ]_{\xi_2} ~|~ a \in D \right \} \tag{IH} \\
				      &= [\forall x P]_{\xi_2}
	    \end{align}

	\item If $[P]_{\xi_1} = [P]_{\xi_2}$ then

	    \begin{align}
		[\exists x P]_{\xi_1} = \text{Similar to the $\forall$ case...}
	    \end{align}
    \end{itemize}
\end{proof}

\begin{definition}
    Let $P$ be a WFF. One defines the notions of

    \begin{itemize}
        \item $P$ satisfied wrt. a structure and an environment
        \item $P$ satisfied wrt. a structure
        \item $P$ satisfiable
        \item $P$ valid
        \item $P$ unsatisfiable
    \end{itemize}
\end{definition}

\begin{theorem}
    Let $\Gamma$ be a set of WFF and let $P$ be a formula, then

    \begin{enumerate}
        \item $P$ is valid iff $\lnot P$ is unsatisfiable
        \item $P$ is satisfiable iff $\lnot P$ is not valid
        \item $\Gamma \vDash P$ iff $\Gamma \cup \left \{ \lnot P \right \}$ is unsatisfiable
    \end{enumerate}
\end{theorem}

\begin{example}
    \begin{equation}
	\exists x \forall y A(x, y) \implies \forall y \exists x A(x, y)
    \end{equation}

    is valid.
\end{example}

\begin{proof}
    \it{\tbd.}
\end{proof}

\begin{theorem}
    Given a WFF $P$

    \begin{itemize}
        \item $P$ is valid iff $\mathrm{Cl}(P)$ is valid
        \item $P$ is satisfiable iff $\mathrm{Ex}(P)$ is valid
    \end{itemize}
\end{theorem}

\begin{theorem}
    Let $P$ be a formula and $z$ a variable not occurring in $P$. Then

    \begin{align}
	\exists X P & \equiv \exists z (P[z/x]) \\
	\forall x P & \equiv \forall z (P[z/x])
    \end{align}
\end{theorem}

\begin{theorem}
    For each WFF $P$, $\exists P'$ in prenex normal form (PNF) such that $P \equiv P'$.
\end{theorem}
