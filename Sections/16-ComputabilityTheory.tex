\section{Computability Theory}

\begin{proposition}
    The function

    \begin{equation}
        \mathrm{eq}(i, j) =
	\begin{cases}
	    1 \quad & \phi_i \equiv \phi_j \\
	    0 \quad & \text{otherwise}
	\end{cases}
    \end{equation}

    is not computable.
\end{proposition}

\begin{proof}
    Suppose $\mathrm{eq}$ was computable, and consider the constant function

    \begin{equation}
        c(x) = 0
    \end{equation}

    which we know to be computable. Let $k$ be an index such that $\phi_k \equiv c$. If $\mathrm{eq}$ was computable, then even the function

    \begin{align}
	f(x) &=
	\begin{cases}
	    1 & \phi_x \equiv \phi_k \\
	    0 & \text{otherwise}
	\end{cases} \\
	 &=
	 \begin{cases}
	     1 & \text{$\phi_x$ is the constant function $0$} \\
	     0 & \text{otherwise}
	 \end{cases}
    \end{align}

    would be computable. But we already knew from Ex. \ref{ex.const.0} that $f$ cannot be computable, so $\mathrm{eq}$ also cannot.
\end{proof}

\begin{proof}[Alternative proof]
    There's an alternative proof in my tablet's notes that I may consider writing down here.
\end{proof}

\begin{example}
    The function

    \begin{equation}
        g(x) =
	\begin{cases}
	    1 \quad & \exists n ~|~ \phi_x(n) \downarrow \\
	    0 \quad & \text{otherwise}
	\end{cases}
    \end{equation}

    is not computable.
\end{example}

\begin{proof}
    Suppose that $g$ was computable, and consider the function

    \begin{equation}
        f(x, y) =
	\begin{cases}
	    1 & \phi_x(x) \downarrow \\
	    \uparrow & \text{otherwise}
	\end{cases}
    \end{equation}

    $f$ is computable, so there has to be an index $k$ such that $\phi_k \equiv f$. By the \smn~theorem $\exists h$, total and computable, such that

    \begin{equation}
	\phi_{h(x)}(y) = \phi_k (x, y) = f(x, y)
    \end{equation}

    Now consider the behavior of $g$ when run on input $h(x)$. We have

    \begin{align}
	g(h(x)) &=
	\begin{cases}
	    1 & \exists n ~|~ \phi_{h(x)}(n) \downarrow \\
	    0 & \text{otherwise}
	\end{cases} \\
	&=
	\begin{cases}
	    1 & \exists n ~|~ f(x, n) \downarrow \\
	    0 & \text{otherwise}
	\end{cases} \\
	&=
	\begin{cases}
	    1 & \phi_x(x) \downarrow \\
	    0 & \text{otherwise}
	\end{cases}
    \end{align}

    This would allow us to decide the set $K = \left \{ x ~|~ \phi_x(x) \downarrow \right \}$, which we know cannot be decided. Hence $g$ cannot be computable.
\end{proof}

\begin{example}
    The function

    \begin{equation}
        g'(x) =
	\begin{cases}
	    1 \quad & \exists n ~|~ \phi_x(n) \downarrow \\
	    \uparrow \quad & \text{otherwise}
	\end{cases}
    \end{equation}

    is computable.
\end{example}

\begin{proof}
    \it{\tbd}.
\end{proof}

\begin{example}
    The function

    \begin{equation}
        g(x) =
	\begin{cases}
	    \mu_n \left \{ \phi_x(n) \downarrow \right \} \quad & \exists n ~|~ \phi_x(n) \downarrow \\
	    \uparrow \quad & \text{otherwise}
	\end{cases}
    \end{equation}

    is not computable.
\end{example}

\begin{proof}
    Suppose $g$ was computable, and consider the function

    \begin{equation}
        f(x, y) =
	\begin{cases}
	    0 & y > 0 \lor \phi_x(x) \downarrow \\
	    \uparrow & \text{otherwise}
	\end{cases}
    \end{equation}

    $f$ is computable, so there has to be an index $k$ such that $\phi_k \equiv f$. By \smn, there exists a total and computable function $h$ such that

    \begin{equation}
        \phi_{h(x)}(y) = f(x, y)
    \end{equation}

    Observe that $f(x, y) \downarrow \forall y \geq 1$, while $f(x, 0) \downarrow \iff \phi_x(x) \downarrow$. Consider now the composition of $g$ and $h$. We have

    \begin{equation}
        g(h(x)) =
	\begin{cases}
	    1 & \phi_x(x) \uparrow \\
	    0 & \phi_x(x) \downarrow
	\end{cases}
    \end{equation}

    which would allow us to decide $K$.
\end{proof}

\begin{theorem}
    If a set $A$ is recursive then it is also RE.
\end{theorem}

\begin{proof}
    If $A = \emptyset$, then $A$ is clearly RE by definition.

    If, instead, $A \neq \emptyset$, then we can \emph{constructively} define an enumeration function $f: \mathbb{N} \to A$ in the following way:

    \begin{enumerate}
        \item Find $k = \mu_x \left \{ c_A(x) = 1 \right \}$, that is the smallest element in $A$
	\item Define

	    \begin{equation}
	        f(x) =
		\begin{cases}
		    x & c_A(x) = 1 \\
		    k & \text{otherwise}
		\end{cases}
	    \end{equation}
    \end{enumerate}
\end{proof}

\begin{proposition}
    Let $A$ and $B$ be two RE sets. Then

    \begin{itemize}
        \item $A \cup B$ is RE
        \item $A \cap B$ is RE
    \end{itemize}
\end{proposition}

\begin{proof}
    \it{\tbd}.
\end{proof}

\begin{theorem}[Post's Theorem]
    \label{thm.post}
    If $A$ is a set such that $A$ and $\overline{A}$ are RE, then $A$ is also recursive.
\end{theorem}

\begin{proof}
    Let $f$ and $f'$ be the enumeration functions of $A$ and $\overline{A}$, respectively. Define the function:

    \begin{equation}
        h(x) =
	\begin{cases}
	    f(x) & \text{$x$ even} \\
	    f'(x) & \text{$x$ odd}
	\end{cases}
    \end{equation}

    Then the decision function $c_A$ for $A$ can be given by:

    \begin{equation}
        c_A(x) = \mathrm{even} \left ( \mu_{x'} \left \{ h(x') = x \right \} \right )
    \end{equation}

    Clearly, given that $A \cup \overline{A} = \mathbb{N}$, given an $x \in \mathbb{N}$, $x \in A$ or $x \in \overline{A}$, so the search of an $x'$ such that $f(x') = x$ must eventually terminate. If such an $x'$ is even, then we know that $f(x') = x$, otherwise it is $f'(x') = x$.
\end{proof}

\begin{theorem}
    A set $A$ is semidecidable iff it is RE.
\end{theorem}

\begin{proof}
    Suppose $A$ is semidecidable and let $h$ be its semidecision function. If $A = \emptyset$, then $A$ is RE by definition.
    If, on the other hand, $A \neq \emptyset$, we can employ a dovetailing technique to enumerate the set of pairs $\left \langle x, t \right \rangle$, where $x$ represents an input and $t$ the number of computation steps that the function $h$ needs to converge on $x$, if it converges on it.
    The enumeration function $f$ for $A$ can then be defined as:

    \begin{equation}
        f(n) = f \left ( \left \langle x, t \right \rangle \right ) =
	\begin{cases}
	    x & \text{if $h(x) \downarrow$ in $t$ steps} \\
	    k & \text{otherwise}
	\end{cases}
    \end{equation}

    where $k$ is an arbitrary element from $A$, which we know has to exist since $A \neq \emptyset$.
    Clearly $f$ is total, for it never diverges, and it is computable, because the statement ``$h(x) \downarrow$ in $t$ steps'' can be computed by Kleene's trinary predicate\footnote{Not covered in this course.}

    For the other direction, suppose $A$ is RE and let $f$ be its total and computable enumeration function. We can then define the following semidecision function for $A$:

    \begin{equation}
        h(x) = \mu_{x'} \left \{ f(x') = x \right \}
    \end{equation}

    If $x \in A$, then this $x$ must eventually be enumerated by $f$ by some input $x'$, that is $\exists x'$ such that $f(x') = x$, and therefore the minimization search initiated by $h(x)$ has to terminate.
    If, on the other hand, $x \notin A$, then the search initiated by $h(x)$ continues indefinitely.
\end{proof}

\begin{proposition}
    The set $K = \left \{ x ~|~ \phi_x(x) \downarrow \right \}$ is RE but not recursive.
\end{proposition}

\begin{proof}
    \it{\tbd}.
\end{proof}

\begin{proposition}
    The set $K' = \left \{ x ~|~ \phi_x(x) \uparrow \right \}$ is not RE.
\end{proposition}

\begin{proof}
    Given that $K' = \overline{K}$, by Post's Theorem \ref{thm.post} we have that, if $K'$ was RE, then $K$ would also be recursive, which we know not to be the case.
\end{proof}

\begin{theorem}[Rice's Theorem]
    An extensional set $A \subseteq \mathbb{N}$ is recursive iff $A = \emptyset$ or $A = \mathbb{N}$.
\end{theorem}

\begin{proof}
    If $A = \emptyset$ or $A = \mathbb{N}$, then $A$ is clearly recursive, for its decision function is either $c_A(x) = 0$ or $c_A(x) = 1$, which is computable in both cases.

    For the other direction, suppose that $A \subseteq \mathbb{N}$ is a decidable, non-trivial set ($A \neq \emptyset$ and $A \neq \mathbb{N}$).
    Given that it is non trivial, $\exists a, b ~|~ a \in A \land b \notin A$. Also, given that its decision function $c_A(x)$ is computable, we can define the total and computable function:

    \begin{align}
	f(x) &=
	\begin{cases}
	    a & x \notin A \\
	    b & x \in A
	\end{cases}
    \end{align}

    Knowing that $f$ is total and computable, by the fixpoint theorem that must exist an index $p$ such that $\phi_{f(p)} \equiv \phi_p$.
    Note that, since $A$ is an extensional set, if $p \in A$ then, given that $\phi_p \equiv \phi_{f(p)}$, $f(p) \in A$ (analogously for $p \notin A$). However, we have that:

    \begin{itemize}
        \item If $p \in A$, $f(p) = b \notin A$ by the definition of $f$
        \item If $p \notin A$, $f(p) = a \in A$ by the definition of $f$
    \end{itemize}

    This is a contradiction, so we must conclude that $A$ can't be an extensional, recursive and non-trivial set at the same time.
\end{proof}

\begin{proposition}
    A formalism that computes only total functions cannot express its own interpreter function.
\end{proposition}

\begin{proof}
    \it{\tbd}.
\end{proof}
